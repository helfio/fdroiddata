Categories:Development
License:GPLv3
Web Site:https://github.com/ligi/BLExplorer/blob/HEAD/README.md
Source Code:https://github.com/ligi/BLExplorer
Issue Tracker:https://github.com/ligi/BLExplorer/issues

Auto Name:BLExplorer
Summary:Bluetooth Low Energy Explorer
Description:
Exlore Bluetooth Low Energy Devices.
.

Repo Type:git
Repo:https://github.com/ligi/BLExplorer.git

Build:1.0,1
    commit=1.0
    subdir=app
    gradle=yes
    prebuild=sed -i '/play_services/d' build.gradle && \
        sed -i '/android-sdk-manager/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
