Categories:Sports & Health
License:GPLv3
Web Site:https://github.com/jonasoreland/runnerup/blob/HEAD/README.md
Source Code:https://github.com/jonasoreland/runnerup/
Issue Tracker:https://github.com/jonasoreland/runnerup/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=runnerup%2eandroid%40gmail%2ecom&lc=US&item_name=RunnerUp&button_subtype=services&currency_code=EUR&tax_rate=25%2e000&bn=PP%2dBuyNowBF%3abtn_buynow_LG%2egif%3aNonHosted

Name:RunnerUp
Summary:Sports activities tracker
Description:
Track your sport activities with RunnerUp using the GPS in your Android phone:

* See detailed stats around your pace, distance and time.
* Get stats and progress through your headphones with built-in highly configurable audio cues.
* Run free runs with target pace or target heart rate zone
* Easily configure and run effective interval workouts modeled after Garmin 410
* Download and run workouts created at Garmin Connect
* Share your favorite workouts with friends
* See yourself and other running LIVE
* Read about others training
* Heart rate monitor: Bluetooth SMART (BLE) for devices that support it, PolarWearLink, Zephyr, ANT+
* Configure and use heart rate zones

Upload your activities to a variety of running sites with a single click:

* RunKeeper
* MapMyRun
* Garmin Connect
* Funbeat
* Nike+
* Endomondo
* jogg.se
* RunningAHEAD
* Digifit
* Strava
* Facebook
* Runtastic
* Google fit
.

Repo Type:git
Repo:https://github.com/jonasoreland/runnerup

Build:1.47,8000047
    disable=remove build
    commit=v1.47
    subdir=app
    submodules=yes
    gradle=latest
    rm=wear
    prebuild=sed -i "/latestCompile 'com.google.android.gms:play-services/d" build.gradle && \
        sed -i "/latestWearApp project(':wear')/d" build.gradle && \
        sed -i "/com.google.android.gms.version/,+2d" latest/AndroidManifest.xml && \
        cp froyo/java/org/runnerup/tracker/component/TrackerWear.java latest/java/org/runnerup/tracker/component/ && \
        cp froyo/java/org/runnerup/widget/AboutPreference.java latest/java/org/runnerup/widget/ && \
        sed -iE "s/shrinkResources true/shrinkResources false/" build.gradle && \
        sed -iE "s/minifyEnabled true/minifyEnabled false/" build.gradle

Build:1.47,14000047
    commit=ac5cce7
    subdir=app
    submodules=yes
    gradle=latest
    rm=wear
    prebuild=sed -i "/latestCompile 'com.google.android.gms:play-services/d" build.gradle && \
        sed -i "/latestWearApp project(':wear')/d" build.gradle && \
        sed -i "/com.google.android.gms.version/,+2d" latest/AndroidManifest.xml && \
        cp froyo/java/org/runnerup/tracker/component/TrackerWear.java latest/java/org/runnerup/tracker/component/ && \
        cp froyo/java/org/runnerup/widget/AboutPreference.java latest/java/org/runnerup/widget/ && \
        sed -i -E "s/shrinkResources true/shrinkResources false/" build.gradle && \
        sed -i -E "s/minifyEnabled true/minifyEnabled false/" build.gradle

Build:1.48,14000048
    commit=v1.48
    subdir=app
    submodules=yes
    gradle=latest
    rm=wear
    prebuild=sed -i "/latestCompile 'com.google.android.gms:play-services/d" build.gradle && \
        sed -i "/latestWearApp project(':wear')/d" build.gradle && \
        sed -i "/com.google.android.gms.version/,+2d" latest/AndroidManifest.xml && \
        cp froyo/java/org/runnerup/tracker/component/TrackerWear.java latest/java/org/runnerup/tracker/component/ && \
        cp froyo/java/org/runnerup/widget/AboutPreference.java latest/java/org/runnerup/widget/ && \
        sed -i -E "s/shrinkResources true/shrinkResources false/" build.gradle && \
        sed -i -E "s/minifyEnabled true/minifyEnabled false/" build.gradle

Build:1.49,14000049
    commit=v1.49
    subdir=app
    submodules=yes
    gradle=latest
    rm=wear
    prebuild=sed -i "/latestCompile 'com.google.android.gms:play-services/d" build.gradle && \
        sed -i "/latestWearApp project(':wear')/d" build.gradle && \
        sed -i "/com.google.android.gms.version/,+2d" latest/AndroidManifest.xml && \
        cp froyo/java/org/runnerup/tracker/component/TrackerWear.java latest/java/org/runnerup/tracker/component/ && \
        cp froyo/java/org/runnerup/widget/AboutPreference.java latest/java/org/runnerup/widget/ && \
        sed -i -E "s/shrinkResources true/shrinkResources false/" build.gradle && \
        sed -i -E "s/minifyEnabled true/minifyEnabled false/" build.gradle

Build:1.50,14000050
    commit=v1.50
    subdir=app
    submodules=yes
    gradle=latest
    rm=wear
    prebuild=sed -i "/latestCompile 'com.google.android.gms:play-services/d" build.gradle && \
        sed -i "/latestWearApp project(':wear')/d" build.gradle && \
        sed -i "/com.google.android.gms.version/,+2d" latest/AndroidManifest.xml && \
        cp froyo/java/org/runnerup/tracker/component/TrackerWear.java latest/java/org/runnerup/tracker/component/ && \
        cp froyo/java/org/runnerup/widget/AboutPreference.java latest/java/org/runnerup/widget/ && \
        sed -i -E "s/shrinkResources true/shrinkResources false/" build.gradle && \
        sed -i -E "s/minifyEnabled true/minifyEnabled false/" build.gradle

Auto Update Mode:None
# Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.50
Current Version Code:14000050
