Categories:System
License:Apache2
Web Site:https://github.com/mattgmg1990/miracast-widget/blob/HEAD/README.md
Source Code:https://github.com/mattgmg1990/miracast-widget
Issue Tracker:https://github.com/mattgmg1990/miracast-widget/issues

Auto Name:Miracast Shortcut
Summary:Miracast widget
Description:
Includes both, a simple activity that simply opens the settings menu to choose a
display/cast your screen and a widget that can open that settings screen with
one click and list the device you are streaming to if Miracast is currently
enabled.
.

Repo Type:git
Repo:https://github.com/mattgmg1990/miracast-widget

Build:1.3,4
    commit=b90f2f645e3b02a456cf2a59fcc410c3036ce773

Build:1.4,5
    commit=9fdcc85102ef270f547ff1fc7ccf7b7cf7887601

Build:1.6,7
    commit=848f1f7d4260c1b5d164ae8fba2ba531f210601d

Build:1.7,8
    commit=b00865c0b7982fc10db936d23b2eb39d430a0684

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.7
Current Version Code:8
