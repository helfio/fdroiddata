Categories:System
License:GPLv3+
Web Site:https://github.com/MrDoomy/Torch/blob/HEAD/README.md
Source Code:https://github.com/MrDoomy/Torch
Issue Tracker:https://github.com/MrDoomy/Torch/issues

Auto Name:Torch
Summary:Flashlight
Description:
Use your phone as a flashlight.

Features:

* Flash camera
* Display illumination
* SOS mode
* Widget flashlight
* Hide icon
.

Repo Type:git
Repo:https://github.com/MrDoomy/Torch

Build:1.0,1
    disable=https://github.com/MrDoomy/Torch/issues/1
    commit=a23d263121e24446b4129fbec31acea726834b38
    forceversion=yes
    forcevercode=yes
    prebuild=mv java src
    target=android-21

Auto Update Mode:None
Update Check Mode:None
