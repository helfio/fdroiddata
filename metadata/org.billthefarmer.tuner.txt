Categories:Multimedia
License:GPLv3
Web Site:https://github.com/billthefarmer/tuner/wiki
Source Code:https://github.com/billthefarmer/tuner
Issue Tracker:https://github.com/billthefarmer/tuner/issues
Changelog:https://github.com/billthefarmer/tuner/releases

Auto Name:Tuner
Summary:Musical instrument pitch tuning
Description:
Accordion tuner with scope, spectrum and strobe. Can show up to eight different
notes or reeds concurrently.
.

Repo Type:git
Repo:https://github.com/billthefarmer/tuner

Build:Version 1.0,1
    commit=v1.0

Build:1.01,101
    commit=v1.01

Build:1.02,102
    commit=v1.02

Build:1.03,103
    commit=v1.03

Build:1.04,104
    commit=v1.04

Build:1.05,105
    commit=v1.05

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.05
Current Version Code:105
